﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CD.BackendService.Models
{
    public class InspectionRectificationAction
    {
        public int InspectionId { get; set; }
        public int CheckNumber { get; set; }
        public string RectificationAction { get; set; }
        public string RectifiedBy { get; set; }

    }
}
