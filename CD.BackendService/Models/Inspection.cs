﻿using System;
using System.Collections.Generic;

namespace CD.BackendService.Models
{
    public partial class Inspection
    {
        public Inspection()
        {
            InspectionResults = new HashSet<InspectionResult>();
            InspectionActions = new HashSet<InspectionRectificationAction>();
        }

        public int InspectionId { get; set; }
        public string Operator { get; set; }
        public string RegistrationNumber { get; set; }
        public long Odometer { get; set; }
        public string MakeModel { get; set; }
        public string Inspector { get; set; }
        public DateTime InspectionDate { get; set; }
        public string SignedName { get; set; }
        public string SignedPosition { get; set; }
        public DateTime SignedDate { get; set; }
        public string Signature { get; set; }

        public ICollection<InspectionResult> InspectionResults { get; set; }
        public ICollection<InspectionRectificationAction> InspectionActions { get; set; }
    }
}
