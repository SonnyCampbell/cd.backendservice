﻿using System;
using System.Collections.Generic;

namespace CD.BackendService.Models
{
    public partial class InspectionResult
    {
        public int InspectionId { get; set; }
        public int CheckNumber { get; set; }
        public sbyte VehicleCondition { get; set; }
        public string DescriptionOfDefect { get; set; }
        public string RectifiedBy { get; set; }

        public InspectionChecklistItem InspectionCheckListItem { get; set; }
        //public Inspections Inspection { get; set; }
    }
}
