﻿using System;
using System.Collections.Generic;

namespace CD.BackendService.Models
{
    public partial class InspectionChecklistItem
    {
        public InspectionChecklistItem()
        {
            //Inspectionresults = new HashSet<Inspectionresults>();
        }

        public int CheckNumber { get; set; }
        public int TmNumber { get; set; }
        public string ItemInspected { get; set; }
        public Section Section { get; set; }

        //public ICollection<Inspectionresults> Inspectionresults { get; set; }
    }

    public enum Section
    {
        InsideCab,
        GroundLevel,
        BrakePerformance
    }
}
