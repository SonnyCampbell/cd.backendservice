﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CD.BackendService.Handlers;
using CD.BackendService.Models;
using CD.BackendService.Repository;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CD.BackendService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<InspectionModelValidator>());
            //services.AddTransient<IValidator<InspectionModel>, InspectionModelValidator>();


            //var repo = new InspectionsRepository("server=localhost;port=3306;database=claredrains;user=root;password=avid3sled;SslMode=none;");
            //services.Add(new ServiceDescriptor(typeof(IInspectionsRepository), repo));
            //services.Add(new ServiceDescriptor(typeof(InspectionsHandler), new InspectionsHandler(repo)));

            services.AddDbContext<ClareDrainsContext>(options => options.UseMySql("server=localhost;database=claredrains;user=root;password=avid3sled;"));
            services.AddScoped<IInspectionsRepository, InspectionsRepository>();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowAll");

            app.UseMvc();
        }
    }
}
