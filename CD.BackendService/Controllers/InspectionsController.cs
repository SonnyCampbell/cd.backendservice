using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CD.BackendService.Models;
using CD.BackendService.Repository;
using Microsoft.AspNetCore.Cors;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CD.BackendService.Controllers
{
    [EnableCors("AllowAll")]
    public class InspectionsController : Controller
    {
        IInspectionsRepository repo;
        public InspectionsController(IInspectionsRepository repository)
        {
            repo = repository;
        }

        // GET api/values
        [HttpGet]
        [Route("api/inspections/{id}")]
        public async Task<Inspection> Get(int id)
        {            
            return await repo.GetInspection(id);
        }

        [HttpGet]
        [Route("api/inspections")]
        public async Task<ActionResult> GetInspections()
        {
            var t = await repo.GetAllInspections();
            var t1 = JsonConvert.SerializeObject(t);
            return Ok(t1);
            //Console.WriteLine(inspectionModel.ToString());
        }

        [HttpPost]
        [Route("api/inspections")]
        public async Task<ActionResult> Post([FromBody]Inspection inspectionModel)
        {
            if (!ModelState.IsValid)
                return new BadRequestObjectResult(ModelState);

            repo.AddInpectionSync(inspectionModel);
            return Ok();
            //Console.WriteLine(inspectionModel.ToString());
        }

        [HttpPut]
        [Route("api/inspections/{id}")]
        public async Task<ActionResult> Put(int id, [FromBody]Inspection inspectionModel)
        {
            //if (!ModelState.IsValid)
            //    return new BadRequestObjectResult(ModelState);


            await repo.UpdateInspection(id, inspectionModel);
            return Ok();
            //Console.WriteLine(inspectionModel.ToString());
        }


        [HttpGet]
        [Route("api/inspectionChecklist")]
        public async Task<IEnumerable<InspectionChecklistItem>> GetInspectionChecklist()
        {
            return await repo.GetInspectionList();
        }
    }
}