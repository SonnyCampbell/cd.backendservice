﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CD.BackendService.Models
{
    public class InspectionModel
    {
        public string Operator { get; set; }
        public string RegNumber { get; set; }
        public int Odometer { get; set; }
        public string MakeModel { get; set; }
        public string Inspector { get; set; }
        public DateTime InspectionDate { get; set; }

        public List<InspectionListItem> InspectionList { get; set; }
    }

    public class InspectionModelValidator : AbstractValidator<InspectionModel>
    {
        public InspectionModelValidator()
        {
            RuleFor(x => x.Operator).NotNull().NotEmpty();
            RuleFor(x => x.RegNumber).NotNull().NotEmpty();
            RuleFor(x => x.MakeModel).NotNull().NotEmpty();
            RuleFor(x => x.Inspector).NotNull().NotEmpty();
            RuleFor(x => x.Odometer).NotNull().GreaterThan(0);
            RuleFor(x => x.InspectionDate).NotNull();
            RuleForEach(x => x.InspectionList).NotNull();

        }
    }
}
