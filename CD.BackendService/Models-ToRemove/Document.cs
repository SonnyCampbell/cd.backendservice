﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CD.BackendService.Models
{
    public class Document
    {
        public int Id { get; set; }
        public string Col1 { get; set; }
        public string Col2 { get; set; }
    }
}
