﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CD.BackendService.Models
{
    public class InspectionListItem
    {
        public int CheckNumber { get; set; }
        public int TMNumber { get; set; }
        public string ItemInspected { get; set; }
        public bool GoodCondition { get; set; }
        public string DescriptionOfDefect { get; set; }
        public string RectifiedBy { get; set; }
    }
}
