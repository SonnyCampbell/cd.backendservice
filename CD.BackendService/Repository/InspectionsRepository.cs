﻿using CD.BackendService.Models;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CD.BackendService.Repository
{
    public class InspectionsRepository : GenericRepository<Inspection>, IInspectionsRepository
    {
        GenericRepository<InspectionChecklistItem> _inspectionListRepo;
        ClareDrainsContext cd;

        public InspectionsRepository(ClareDrainsContext dbContext)
        : base(dbContext)
        {
            cd = dbContext;
            _inspectionListRepo = new GenericRepository<InspectionChecklistItem>(dbContext);
        }

        //public string ConnectionString { get; set; }

        //public InspectionsRepository(string connectionString)
        //{
        //    ConnectionString = connectionString;
        //}

        //private MySqlConnection GetConnection()
        //{
        //    return new MySqlConnection(ConnectionString);
        //}

        ///// <summary>
        ///// Get the checklist of all items to be inspected
        ///// </summary>
        ///// <returns></returns>
        //public List<InspectionListItem> GetInspectionList()
        //{
        //    List<InspectionListItem> list = new List<InspectionListItem>();

        //    using (MySqlConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        MySqlCommand cmd = new MySqlCommand("select * from inspectionlist", conn);

        //        using (var reader = cmd.ExecuteReader())
        //        {
        //            while (reader.Read())
        //            {
        //                list.Add(new InspectionListItem()
        //                {
        //                    CheckNumber = Convert.ToInt32(reader["checkNumber"]),
        //                    TMNumber = Convert.ToInt32(reader["tmNumber"]),
        //                    ItemInspected = reader["itemInspected"].ToString(),
        //                    GoodCondition = true,
        //                    DescriptionOfDefect = "",
        //                    RectifiedBy = ""
        //                });
        //            }
        //        }
        //    }
        //    return list;
        //}

        ///// <summary>
        ///// Insert an inspection report
        ///// </summary>
        ///// <param name="inspectionModel"></param>
        //public void AddInspection(InspectionModel inspectionModel)
        //{
        //    using (MySqlConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        MySqlCommand cmd = conn.CreateCommand();
        //        MySqlTransaction trans = conn.BeginTransaction();
        //        cmd.Transaction = trans;
        //        try
        //        {
        //            cmd.CommandText = "INSERT INTO inspections (operator, registrationNumber, odometer, makeModel, inspector, inspectionDate) " +
        //                "VALUES (@operator, @registrationNumber, @odometer, @makeModel, @inspector, @inspectionDate); ";

        //            cmd.Parameters.AddWithValue("@operator", inspectionModel.Operator);
        //            cmd.Parameters.AddWithValue("@registrationNumber", inspectionModel.RegNumber);
        //            cmd.Parameters.AddWithValue("@odometer", inspectionModel.Odometer);
        //            cmd.Parameters.AddWithValue("@makeModel", inspectionModel.MakeModel);
        //            cmd.Parameters.AddWithValue("@inspector", inspectionModel.Inspector);
        //            cmd.Parameters.AddWithValue("@inspectionDate", inspectionModel.InspectionDate);
        //            cmd.ExecuteNonQuery();



        //            cmd.Parameters.AddWithValue("@lastInsertID", cmd.LastInsertedId);
        //            cmd.CommandText = "INSERT INTO inspectionresults (inspectionId, checkNumber, vehicleGoodCondition, descriptionOfDefect, rectifiedBy) VALUES ";

        //            foreach (var item in inspectionModel.InspectionList)
        //            {
        //                cmd.CommandText +=
        //                    " (@lastInsertID, " +
        //                            "@checkNumber" + item.CheckNumber + ", " +
        //                            "@vehicleGoodCondition" + item.CheckNumber + ", " +
        //                            "@descriptionOfDefect" + item.CheckNumber + ", " +
        //                            "@rectifiedBy" + item.CheckNumber + "),";

        //                cmd.Parameters.AddWithValue("@checkNumber" + item.CheckNumber, item.CheckNumber);
        //                cmd.Parameters.AddWithValue("@vehicleGoodCondition" + item.CheckNumber, item.GoodCondition);
        //                cmd.Parameters.AddWithValue("@descriptionOfDefect" + item.CheckNumber, item.DescriptionOfDefect);
        //                cmd.Parameters.AddWithValue("@rectifiedBy" + item.CheckNumber, item.RectifiedBy);
        //            }

        //            cmd.CommandText = cmd.CommandText.Substring(0, cmd.CommandText.Length - 1);
        //            cmd.CommandText += ";";
        //            cmd.ExecuteNonQuery();

        //            trans.Commit();

        //        }
        //        catch (Exception e)
        //        {
        //            try
        //            {
        //                trans.Rollback();
        //            }
        //            catch (MySqlException ex)
        //            {
        //                if (trans.Connection != null)
        //                {
        //                    Console.WriteLine("An exception of type " + ex.Message +
        //                    " was encountered while attempting to roll back the transaction.");
        //                }
        //            }

        //            Console.WriteLine("Failed to commit transaction to DB: " + e.Message);
        //        }
        //        finally
        //        {
        //            conn.Close();
        //        }

        //    }
        //}

        //public InspectionModel GetInspection(int inspectionId)
        //{
        //    InspectionModel inspection = new InspectionModel();

        //    using (MySqlConnection conn = GetConnection())
        //    {
        //        conn.Open();
        //        MySqlCommand cmd = conn.CreateCommand();

        //        cmd.CommandText = "SELECT * from inspections where inspectionId = @inspectionId";
        //        cmd.Parameters.AddWithValue("@inspectionId", inspectionId);
        //        var rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            inspection.Inspector = rdr["inspector"].ToString();
        //            inspection.InspectionDate = Convert.ToDateTime(rdr["inspectionDate"]);
        //            inspection.MakeModel = rdr["makeModel"].ToString();
        //            inspection.RegNumber = rdr["registrationNumber"].ToString();
        //            inspection.Odometer = Convert.ToInt32(rdr["odometer"]);
        //            inspection.Operator = rdr["operator"].ToString();
        //            inspection.InspectionList = new List<InspectionListItem>();
        //        }
        //        rdr.Close();

        //        cmd.CommandText = "select vehicleCondition, descriptionOfDefect, tmNumber, rectifiedBy, itemInspected, ir.checkNumber as checkNumber from inspectionresults ir " +
        //            "join inspectionlist il on ir.checkNumber = il.checkNumber " +
        //            "where ir.inspectionId =  @inspectionId";
        //        rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            InspectionListItem nextItem = new InspectionListItem();
        //            nextItem.GoodCondition = Convert.ToBoolean(rdr["vehicleGoodCondition"]);
        //            nextItem.DescriptionOfDefect = rdr["descriptionOfDefect"].ToString();
        //            nextItem.TMNumber = Convert.ToInt32(rdr["tmNumber"]);
        //            nextItem.RectifiedBy = rdr["rectifiedBy"].ToString();
        //            nextItem.ItemInspected = rdr["itemInspected"].ToString();
        //            nextItem.CheckNumber = Convert.ToInt32(rdr["checkNumber"]);
        //            inspection.InspectionList.Add(nextItem);

        //        }
        //    }

        //    return inspection;
        //}

        public async Task<int> AddInspection(Inspection entity)
        {
            return await Create(entity);
        }

        public int AddInpectionSync(Inspection entity)
        { 
            return CreateSync(entity);
        }

        public async Task<int> UpdateInspection(int id, Inspection entity)
        {
            var t = await GetAll()
                    .Where(x => x.InspectionId == id)
                    .Include(x => x.InspectionResults)
                    .Include(x => x.InspectionActions)
                    .FirstOrDefaultAsync();

            cd.Entry(t).CurrentValues.SetValues(entity);

            foreach(var item in entity.InspectionResults)
            {
                var originalItem = t.InspectionResults
                                    .Where(x => x.InspectionId == id 
                                            && x.CheckNumber == item.CheckNumber)
                                    .SingleOrDefault();

                cd.Entry(originalItem).CurrentValues.SetValues(item);
                cd.Entry(originalItem).State = EntityState.Modified;

            }

            foreach (var item in entity.InspectionActions)
            {
                var originalItem = t.InspectionResults
                                    .Where(x => x.InspectionId == id
                                            && x.CheckNumber == item.CheckNumber)
                                    .SingleOrDefault();

                cd.Entry(originalItem).CurrentValues.SetValues(item);
                cd.Entry(originalItem).State = EntityState.Modified;

            }

            cd.Entry(t).State = EntityState.Modified;
            return await cd.SaveChangesAsync();

        }

        public Task<Inspection> GetInspection(int inspectionId)
        {
            return GetAll()
                    .Where(x => x.InspectionId == inspectionId)
                    .Include(x => x.InspectionResults)
                    .ThenInclude(x => x.InspectionCheckListItem)
                    .Include(x => x.InspectionActions)
                    .FirstOrDefaultAsync();
        }

        public Task<List<Inspection>> GetAllInspections()
        {
            return GetAll()
                    .Include(x => x.InspectionResults)
                    .ThenInclude(y => y.InspectionCheckListItem)
                    .Include(x => x.InspectionActions)
                    .ToListAsync();
        }

        public Task<List<InspectionChecklistItem>> GetInspectionList()
        {
            return _inspectionListRepo.GetAll().ToListAsync();
        }
    }
}
