﻿using CD.BackendService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CD.BackendService.Repository
{
    public interface IInspectionsRepository : IGenericRepository<Inspection>
    {
        Task<List<InspectionChecklistItem>> GetInspectionList();
        Task<int> AddInspection(Inspection entity);
        int AddInpectionSync(Inspection entity);
        Task<Inspection> GetInspection(int inspectionId);
        Task<List<Inspection>> GetAllInspections();
        Task<int> UpdateInspection(int id, Inspection entity);
    }
}
