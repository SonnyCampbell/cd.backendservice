﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CD.BackendService.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly ClareDrainsContext _dbContext;

        public GenericRepository(ClareDrainsContext dbContext)
        {
            _dbContext = dbContext;
            
        }

        public async Task<int> Create(TEntity entity)
        {
            
            await _dbContext.Set<TEntity>().AddAsync(entity);
            //return await _dbContext.SaveChangesAsync();
            return _dbContext.SaveChanges();
        }

        public int CreateSync(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
            //return await _dbContext.SaveChangesAsync();
            return _dbContext.SaveChanges();
        }

        //public async Task Delete(int id)
        //{
        //    var entity = await GetById(id);
        //    _dbContext.Set<TEntity>().Remove(entity);
        //    await _dbContext.SaveChangesAsync();
        //}

        public IQueryable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().AsNoTracking();
        }

        //public async Task<TEntity> GetById(int id)
        //{
        //    return await _dbContext.Set<TEntity>()
        //        .AsNoTracking()
        //        .FirstOrDefaultAsync(e => e.Id == id);
        //}

        public virtual void Update(int id, TEntity entity)
        {
            var t =_dbContext.Set<TEntity>().Find(id);
            _dbContext.Entry(t).CurrentValues.SetValues(entity);
            _dbContext.Entry(t).State = EntityState.Modified;


            //_dbContext.Set<TEntity>().Update(entity);
            _dbContext.SaveChanges();
            return;
        }
    }
}
