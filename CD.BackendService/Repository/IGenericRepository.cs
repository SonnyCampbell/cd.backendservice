﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CD.BackendService.Repository
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();

        //Task<TEntity> GetById(int id);

        Task<int> Create(TEntity entity);

        void Update(int id, TEntity entity);

        //Task Delete(int id);
    }
}
