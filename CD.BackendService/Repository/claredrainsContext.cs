﻿using System;
using CD.BackendService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Logging;

namespace CD.BackendService.Repository
{
    public partial class ClareDrainsContext : DbContext
    {
        public ClareDrainsContext()
        {
        }

        public ClareDrainsContext(DbContextOptions<ClareDrainsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<InspectionChecklistItem> Inspectionlist { get; set; }
        public virtual DbSet<InspectionResult> Inspectionresults { get; set; }
        public virtual DbSet<Inspection> Inspections { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var lf = new LoggerFactory();
            lf.AddProvider(new LoggerProvider());
            optionsBuilder.UseLoggerFactory(lf);

            if (!optionsBuilder.IsConfigured)
            {
                

                optionsBuilder.UseMySql("server=localhost;database=claredrains;user=root;password=avid3sled;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InspectionChecklistItem>(entity =>
            {
                entity.HasKey(e => e.CheckNumber);

                entity.ToTable("inspectionlist");

                entity.Property(e => e.CheckNumber)
                    .HasColumnName("checkNumber")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ItemInspected)
                    .IsRequired()
                    .HasColumnName("itemInspected")
                    .HasMaxLength(100);

                entity.Property(e => e.TmNumber)
                    .HasColumnName("tmNumber")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Section)
                    .HasColumnName("section")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<InspectionResult>(entity =>
            {
                entity.HasKey(e => new { e.InspectionId, e.CheckNumber });

                entity.ToTable("inspectionresults");

                entity.HasIndex(e => e.CheckNumber)
                    .HasName("checkNumber");

                entity.Property(e => e.InspectionId)
                    .HasColumnName("inspectionId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CheckNumber)
                    .HasColumnName("checkNumber")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DescriptionOfDefect)
                    .IsRequired()
                    .HasColumnName("descriptionOfDefect")
                    .HasMaxLength(1000);

                entity.Property(e => e.RectifiedBy)
                    .HasColumnName("rectifiedBy")
                    .HasMaxLength(100);

                entity.Property(e => e.VehicleCondition)
                    .HasColumnName("vehicleGoodCondition")
                    .HasColumnType("tinyint(4)");

                //entity.HasOne(d => d.CheckNumberNavigation)
                //    .WithMany(p => p.Inspectionresults)
                //    .HasForeignKey(d => d.CheckNumber)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("inspectionresults_ibfk_2");

                //entity.HasOne(d => d.Inspection)
                //    .WithMany(p => p.Inspectionresults)
                //    .HasForeignKey(d => d.InspectionId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("inspectionresults_ibfk_1");
            });

            modelBuilder.Entity<InspectionRectificationAction>(entity =>
            {
                entity.HasKey(e => new { e.InspectionId, e.CheckNumber });

                entity.ToTable("inspectionaction");

                entity.HasIndex(e => e.CheckNumber)
                    .HasName("checkNumber");

                entity.Property(e => e.InspectionId)
                    .HasColumnName("inspectionId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CheckNumber)
                    .HasColumnName("checkNumber")
                    .HasColumnType("int(11)");

                entity.Property(e => e.RectificationAction)
                    .IsRequired()
                    .HasColumnName("rectificationAction")
                    .HasMaxLength(1000);

                entity.Property(e => e.RectifiedBy)
                    .HasColumnName("rectifiedBy")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Inspection>(entity =>
            {
                entity.HasKey(e => e.InspectionId);

                entity.ToTable("inspections");

                entity.Property(e => e.InspectionId)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("inspectionId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Inspector)
                    .IsRequired()
                    .HasColumnName("inspector")
                    .HasMaxLength(100);

                entity.Property(e => e.MakeModel)
                    .IsRequired()
                    .HasColumnName("makeModel")
                    .HasMaxLength(100);

                entity.Property(e => e.Odometer)
                    .HasColumnName("odometer")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Operator)
                    .IsRequired()
                    .HasColumnName("operator")
                    .HasMaxLength(100);

                entity.Property(e => e.RegistrationNumber)
                    .IsRequired()
                    .HasColumnName("registrationNumber")
                    .HasMaxLength(45);

                entity.Property(e => e.InspectionDate)
                    .IsRequired()
                    .HasColumnName("inspectionDate")
                    .HasColumnType("Date");

                entity.Property(e => e.SignedName)
                    .IsRequired()
                    .HasColumnName("signedName")
                    .HasMaxLength(100);

                entity.Property(e => e.SignedPosition)
                    .IsRequired()
                    .HasColumnName("signedPosition")
                    .HasMaxLength(100);

                entity.Property(e => e.Signature)
                    .IsRequired()
                    .HasColumnName("signature")
                    .HasMaxLength(100);

                entity.Property(e => e.SignedDate)
                    .IsRequired()
                    .HasColumnName("signedDate")
                    .HasColumnType("Date");
            });
        }
    }
}
